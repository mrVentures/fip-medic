#include "pch.h" // This line must be the first line

#include "leptonica/src/allheaders.h"
#include "tesseract/baseapi.h"
#include <iostream>
#include <map>
#include <unordered_map>
#include <set>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <filesystem>
#include <utility>
#include <assert.h>
//#include <windows.h>
#include <algorithm>
#include <cctype>
#include <filesystem>
namespace fs = std::filesystem;

// Ibuprofen label: https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=93edca03-fdf1-453f-8efe-5f0b8eef41e4
// Nyquil label: https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=0fe37f2e-cf13-451d-8988-e96a5712e359
const std::string kListOfTermsPath = "Data/Terms.txt";
const std::string kExampleTextPath = "Data/text/";

const std::string kInputImageDirectory = "Data/input_images/";
const std::string kCombinedTextDirectory = "Data/combined_text/";
const std::string kInputImageFilePaths = "Data/image_paths.txt";
const std::string kOutputFilePath = "Data/output_instructions.txt";
const std::string kWordVectorPath = "Data/word_vecs.txt";
const std::string kSynonymsPath = "Data/synonyms.txt";
const std::string kSpace = " ";
const int kNumMedications = 43;
const int kWordVecLength = 300;
const bool kUseWordVectors = false;
const bool kUseTesseract = true;
const bool kUseSynonyms = true;
const bool kRunImagesToText = true;

#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

enum TermCategory
{
	DoseRoute,
	Frequency,
	Indications,
	Precautions,
	SideEffects
};

typedef std::map< TermCategory, std::set< std::string > > terms;

std::unordered_map< std::string, TermCategory> kTextToTermCategory = {
	{"DoseRoute", DoseRoute},
	{"Frequency", Frequency},
	{"Indications", Indications},
	{"Precautions", Precautions},
	{"SideEffects", SideEffects},
};

std::unordered_map< TermCategory, std::string> kTermCategoryToText = {
	{DoseRoute, "DoseRoute"},
	{Frequency, "Frequency"},
	{Indications, "Indications"},
	{Precautions, "Precautions"},
	{SideEffects, "SideEffects"},
};

terms CreateNewTerms() {
	terms emptyTerms;
	for (int currTermCategoryIndex = DoseRoute; currTermCategoryIndex != SideEffects; currTermCategoryIndex++)
	{
		TermCategory currTermCategory = (TermCategory)currTermCategoryIndex;
		std::set< std::string> emptySet = {};
		emptyTerms.insert({ currTermCategory, emptySet });
	}
	return emptyTerms;
}

void CombineOutputText(const std::vector<std::string> &tmpFiles, const std::string &medicationName) {
	std::string combinedFilePath = kCombinedTextDirectory + medicationName + ".txt";
	std::ofstream combinedFile(combinedFilePath);
	ASSERT(combinedFile.is_open(), "Error opening combined file: " + combinedFilePath);

	// loop over each temporary file associated with one drug
	for (const std::string file : tmpFiles) {
		std::ifstream tmpfile(file + ".txt");
		ASSERT(tmpfile.is_open(), "Error opening temporary tesseract text files: " + file + ".txt");
		std::string line;

		// copy over every line from each file into the same output file
		while (std::getline(tmpfile, line)) {
			combinedFile << line + "\n";
		}
	}

	combinedFile.close();
}

void ImagesToText(const std::string &translationLanguage) {
	// Collect paths sorted by each example
	// < exampleName, vector< imagePath > >
	typedef std::unordered_map<std::string, std::vector< std::string>> exampleNameToImagePaths;

	exampleNameToImagePaths imagePaths;
	for (const auto & entry : fs::directory_iterator(kInputImageDirectory)) {
		std::string const curPath = entry.path().u8string();
		ASSERT(curPath.find("/") != -1, "Error: fatal file path format, /");
		ASSERT(curPath.find("_") != -1, "Error: fatal file path format, _");

		// Data/input_images/9_tylenol_8hr_3.jpg --> 9_tylenol_8hr
		std::string exampleName = curPath.substr(curPath.find_last_of("/") + 1);
		exampleName = exampleName.substr(0, exampleName.find_last_of("_"));

		const bool firstImageOfThisExample = (imagePaths.find(exampleName) == imagePaths.end());
		if (firstImageOfThisExample) {
			imagePaths[exampleName] = std::vector< std::string>();
		}

		imagePaths[exampleName].push_back(curPath);
	}

	// Loop through images of each example and generate text
	for (exampleNameToImagePaths::iterator it = imagePaths.begin(); it != imagePaths.end(); ++it) {
		std::string exampleName = it->first;
		std::vector< std::string> imagePaths = it->second;
		std::vector<std::string> tmpFiles;

		if (kUseTesseract) {

			// TODO Clear folder of generated text
			// system("exec rm -r Data/generated_text/*");

			// TODO Clear folder of combined text
			// system("exec rm -r Data/combined_text/*");
			tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
			ASSERT(!api->Init(NULL, translationLanguage.c_str()), "Failed to initailize Tesseract API");

			// Call tesseract on all the images associated with current medication example
			for (int imageIndex = 0; imageIndex < imagePaths.size(); imageIndex++) {
				std::string curInputImagePath = imagePaths[imageIndex];
				std::string outputFilePath = "Data/generated_text/" + exampleName + "_" + std::to_string(imageIndex);
				
				// initialize image
				Pix *image = pixRead(curInputImagePath.c_str());
    			api->SetImage(image);
				char *outText = api->GetUTF8Text();

				// write output
				std::ofstream output(outputFilePath + ".txt");
				ASSERT(output.is_open(), "output file could not open");
				output << outText;
				// output.close();

				// free memory
				delete [] outText;
    			pixDestroy(&image);
			
				tmpFiles.push_back(outputFilePath);
			}

			api->End();
		}

		CombineOutputText(tmpFiles, exampleName);
		tmpFiles.clear();
	}
}

terms ReadInAllTerms() {
	terms allTerms = CreateNewTerms();

	std::ifstream file(kListOfTermsPath);
	std::string str;
	while (std::getline(file, str))
	{
		const int indexOfSlash = str.find("/");
		ASSERT(indexOfSlash != -1, "");

		std::string category = str.substr(0, indexOfSlash);
		std::string text = str.substr(indexOfSlash + 1);

		ASSERT(kTextToTermCategory.count(category) > 0, "No term category found for: " + category);
		TermCategory currCategory = kTextToTermCategory[category];

		transform(text.begin(), text.end(), text.begin(), ::tolower);
		allTerms[currCategory].insert(text);
	}

	file.close();

	return allTerms;
}

void StandardizeFrequencies(std::string &medicationText) {
	std::vector<int> indexes;

	// get all occurances of the word " hour" 
	int hoursIndex = medicationText.find(" hour");
	while (hoursIndex != std::string::npos) {
		indexes.push_back(hoursIndex);
		hoursIndex = medicationText.find(" hour", hoursIndex + 1);
	}

	// for each place, check to see if a range was given and if it was, take the smallest
	for (int i = indexes.size() - 1; i >= 0; i--) {
		hoursIndex = indexes[i];

		int spaceIndex = medicationText.substr(0, hoursIndex).rfind(" ");
		if (spaceIndex == -1) continue;

		std::string to = " to";
		int toIndex = medicationText.substr(0, spaceIndex).rfind(" to");
		if (toIndex == -1 || toIndex + to.length() != spaceIndex) continue; // to was not found or not found right before

		std::string minArea = medicationText.substr(0, toIndex);
		int secondSpaceIndex = minArea.rfind(" ");
		if (secondSpaceIndex == -1) continue; // this does not take edge cases into account (line wrap around, starting word) but maybe this is ok? 

		medicationText = medicationText.substr(0, toIndex) + medicationText.substr(hoursIndex);
	}
}


bool invalidChar(char c) {
	return !((int)c >= 0 && (int)c < 128);
}
void stripUnicode(std::string & str) {
	str.erase(remove_if(str.begin(), str.end(), invalidChar), str.end());
}

void CleanText(std::string &text) {
	std::string cleanText = "";

	for (const auto letter : text) {
		if (std::isalnum(letter) || letter == ' ') {
			cleanText += letter;
		}
	}

	text = cleanText;
}

std::string ReadInExampleText(const std::string examplePath) {
	std::ifstream file(examplePath);

	std::string wholeFile;
	std::string str;
	size_t spaceCount = 0;

	while (std::getline(file, str))
	{
		stripUnicode(str);
		transform(str.begin(), str.end(), str.begin(), ::tolower);
		wholeFile += str;
	}

	StandardizeFrequencies(wholeFile);
	CleanText(wholeFile);
	file.close();

	return wholeFile;
}

void PrintTerms(terms termsToPrint, std::ofstream &output) {
	for (int currTermCategoryIndex = DoseRoute; currTermCategoryIndex <= SideEffects; currTermCategoryIndex++)
	{
		TermCategory currTermCategory = (TermCategory)currTermCategoryIndex;
		output << "=======>  " << kTermCategoryToText[currTermCategory] << std::endl;

		std::set< std::string> termsOfCurrCategory = termsToPrint[currTermCategory];
		bool atLeastOne = false;
		auto currTerm = termsOfCurrCategory.begin();
		if (currTerm != termsOfCurrCategory.end()) {
			output << *currTerm;
			atLeastOne = true;
			++currTerm;
		}

		for (; currTerm != termsOfCurrCategory.end(); ++currTerm) {
			output << ", " << *currTerm;
		}

		if (!atLeastOne)
			output << "--None--";
		output << "\n";
	}
}

std::string GetQuantityTerm(const std::string &medicationText, const std::string &currTerm) {
	int index = currTerm.find("_");
	ASSERT(index != -1, "Error: current term does not have a '_' : " + currTerm);

	std::string newTerm = currTerm.substr(index + 1);
	int termIndex = medicationText.find(newTerm);
	if (termIndex != -1) {
		int spaceIndex = medicationText.substr(0, termIndex).rfind(" ");
		if (spaceIndex != -1) {
			std::string quantity = medicationText.substr(spaceIndex + 1, termIndex - spaceIndex - 1);

			if (quantity == "") return "";
			for (const auto letter : quantity) if (!std::isdigit(letter)) return "";

			return quantity + newTerm;
		}
	}

	return "";
}

terms ExtractTermsBySubstringSearch(terms allTerms, std::string medicationText) {
	terms extractedTerms = CreateNewTerms();
	for (int currTermCategoryIndex = DoseRoute; currTermCategoryIndex != SideEffects; currTermCategoryIndex++)
	{
		TermCategory currTermCategory = (TermCategory)currTermCategoryIndex;
		std::set< std::string> termsOfCurrCategory = allTerms[currTermCategory];
		for (auto currTermIter = termsOfCurrCategory.begin(); currTermIter != termsOfCurrCategory.end(); ++currTermIter) {
			std::string currTerm = *currTermIter;

			if (currTerm.find('_') != -1) {
				currTerm = GetQuantityTerm(medicationText, currTerm);
				if (currTerm.length() != 0) extractedTerms[currTermCategory].insert(currTerm);
			}
			else if (medicationText.find(currTerm) != -1) {
				extractedTerms[currTermCategory].insert(currTerm);
			}
		}
	}

	return extractedTerms;
}

double Norm(const double arr[]) {
	double sum = 0.0;
	for (size_t i = 0; i < kWordVecLength; i++) {
		sum += pow(arr[i], 2);
	}

	return sqrt(sum);
}

double DotProduct(const double arr1[], const double arr2[]) {
	double sum = 0.0;
	for (size_t i = 0; i < kWordVecLength; i++) {
		sum += arr1[i] * arr2[i];
	}

	return sum;
}

void Add(double arr1[], const double arr2[]) {
	for (size_t i = 0; i < kWordVecLength; i++) {
		arr1[i] += arr2[i];
	}
}

void Divide(double arr[], double divisor) {
	for (size_t i = 0; i < kWordVecLength; i++) {
		arr[i] /= divisor;
	}
}

double CosineSimilarity(const double arr1[], const double arr2[]) {
	return DotProduct(arr1, arr2) / (Norm(arr1) * Norm(arr2));
}

void ZeroIntialize(double arr[]) {
	for (size_t i = 0; i < kWordVecLength; i++) {
		arr[i] = 0;
	}
}

std::map<std::string, double[kWordVecLength]> ReadInWordVectors() {
	std::map<std::string, double[kWordVecLength]> wordVecs;
	std::ifstream vecs(kWordVectorPath);
	ASSERT(vecs.is_open(), "Could not open word vector file");

	std::string line;

	while (std::getline(vecs, line)) {
		int firstSpaceIndex = line.find(" ");
		ASSERT(firstSpaceIndex != -1, "Error: word vector file improperly formatted.");

		std::string word = line.substr(0, firstSpaceIndex);
		int count = 0;
		int nextSpaceIndex = line.find(" ", firstSpaceIndex + 1);

		// continually extract each element of the vector 
		while (nextSpaceIndex != -1) {
			double num = std::stod(line.substr(firstSpaceIndex + 1, nextSpaceIndex - firstSpaceIndex));
			wordVecs[word][count] = num;
			firstSpaceIndex = nextSpaceIndex;
			nextSpaceIndex = line.find(" ", firstSpaceIndex + 1);
			count++;
		}

		ASSERT(count == kWordVecLength - 1, "Error: wrong number of elements read into vector");

		wordVecs[word][count] = stod(line.substr(firstSpaceIndex + 1));
	}

	vecs.close();
	return wordVecs;
}

std::vector<std::string> Split(const std::string &text) {
	std::string token = "";
	std::vector<std::string> words;

	// split on space
	for (const auto letter : text) {
		if (letter == ' ') {
			if (token != "") words.push_back(token);
			token = "";
		}
		else {
			token += letter;
		}
	}
	words.push_back(token);

	return words;
}

void CalcPhraseVector(double phraseVec[], const std::vector<std::string> &words, const std::map<std::string, double[kWordVecLength]> &wordVecs) {
	ASSERT(words.size() >= 1, "Error: words vector empty");
	ZeroIntialize(phraseVec);
	for (size_t i = 0; i < words.size(); i++) {
		std::string word = words[i];
		//std::cout << "word: " << word << std::endl;
		if (wordVecs.find(word) == wordVecs.end()) continue;
		Add(phraseVec, wordVecs.at(word));
	}

	Divide(phraseVec, words.size());
}

terms ExtractTermsByWordVectorSearch(terms &allTerms, std::string &medicationText, const std::map<std::string, double[kWordVecLength]> &wordVecs) {
	std::vector<std::string> medicationTextWords = Split(medicationText);
	terms extractedTerms = CreateNewTerms();

	for (int currTermCategoryIndex = DoseRoute; currTermCategoryIndex != SideEffects; currTermCategoryIndex++)
	{
		TermCategory currTermCategory = (TermCategory)currTermCategoryIndex;
		std::set< std::string> termsOfCurrCategory = allTerms[currTermCategory];
		for (auto currTermIter = termsOfCurrCategory.begin(); currTermIter != termsOfCurrCategory.end(); ++currTermIter) {
			std::string currTerm = *currTermIter;

			std::vector<std::string> termWords = Split(currTerm);
			double termVec[kWordVecLength];
			CalcPhraseVector(termVec, termWords, wordVecs);

			if (currTerm == "_ ml" || currTerm == "_ capsules" || currTerm == "_ tablets") {
				currTerm = GetQuantityTerm(medicationText, currTerm);
				if (currTerm.length() != 0) extractedTerms[currTermCategory].insert(currTerm);
			}
			else {
				size_t phraseSize = termWords.size();
				for (size_t i = 0; i < medicationTextWords.size() - phraseSize; i += phraseSize) {
					std::vector<std::string> phraseWords;
					for (size_t j = 0; j < phraseSize; j++) {
						phraseWords.push_back(medicationTextWords[i + j]);
					}

					double phraseVec[kWordVecLength];
					CalcPhraseVector(phraseVec, phraseWords, wordVecs);
					if (CosineSimilarity(termVec, phraseVec) > 0.9) {
						//std::cout << "Adding term: " << currTerm << " because of: ";
						//for (size_t a = 0; a < phraseWords.size(); a++) std::cout << phraseWords[a] << " ";
						//td::cout << std::endl;
						extractedTerms[currTermCategory].insert(currTerm);
					}
				}
			}
		}
	}
	return extractedTerms;
}

std::map<std::string, std::string> ReadInSynonyms() {
	std::map<std::string, std::string> synonyms;
	std::ifstream file(kSynonymsPath);
	ASSERT(file.is_open(), "Error: could not open synonyms file.");
	std::string line;

	while(std::getline(file, line)) {
		int commaIndex = line.find(',');
		ASSERT(commaIndex != -1, "Error: synonyms file not properly formatted");
		std::string synonym = line.substr(0, commaIndex);
		std::string groundTruthWord = line.substr(commaIndex + 1);
		synonyms[synonym] = groundTruthWord;
	}

	file.close();

	return synonyms;
}

void TranslateSynonyms(std::string &text, const std::map<std::string, std::string> &synonyms) {
	for (const auto iterator : synonyms) {
		std::string synonym = iterator.first;
		std::string groundTruthWord = iterator.second;
		int wordIndex = text.find(synonym);
		while (wordIndex != -1) {
			text = text.substr(0, wordIndex) + groundTruthWord + text.substr(wordIndex + synonym.length());
			wordIndex = text.find(synonym, wordIndex + groundTruthWord.length());
		}
	}
}

terms ExtractTermsFromText(std::ofstream &output, std::string path, std::string name, const terms &allTerms, const std::map<std::string, std::string> &synonyms) {
	std::string text = ReadInExampleText(path);
	if (kUseSynonyms)
		TranslateSynonyms(text, synonyms);

	terms extractedTerms;
	if (kUseWordVectors) {
		//extractedTerms = ExtractTermsByWordVectorSearch(allTerms, text, wordVecs);
	}
	else {
		extractedTerms = ExtractTermsBySubstringSearch(allTerms, text);
	}

	// Print to console and output file
	output << "Example " << name << " extracted terms...\n";
	PrintTerms(extractedTerms, output);
	output << "\n\n";

	return extractedTerms;
}

void EvalAllExamples(const terms & allTerms) {
	// Save example images as text
	if (kRunImagesToText) {
		std::string translationLanguage = "eng";
		ImagesToText(translationLanguage);
	}

	// Init word vectors if needed
	std::map<std::string, double[kWordVecLength]> wordVecs;
	if (kUseWordVectors)
		wordVecs = ReadInWordVectors();

	// Init synonyms if needed
	std::map<std::string, std::string> synonyms;
	if (kUseSynonyms)
		synonyms = ReadInSynonyms();
	// Extract and evaluate terms from each example


	std::ofstream output(kOutputFilePath);

	ASSERT(output.is_open(), "output file could not open");
	for (const auto & entry : fs::directory_iterator(kCombinedTextDirectory)) {
		std::string const curPath = entry.path().u8string();

		// Data/combined_text/5_childrens_advil.txt --> 5_childrens_advil
		std::string curName = curPath.substr(curPath.find_last_of("/") + 1);
		curName = curName.substr(0, curName.find_last_of("."));

		terms generatedTerms = ExtractTermsFromText(output, curPath, curName, allTerms, synonyms);
		terms oracleTerms; // TODO
		// CompareTerms(generatedTerms, oracleTerms);
	}
	output.close();
}

int main() {
	const terms allTerms = ReadInAllTerms();
	EvalAllExamples(allTerms);
}
