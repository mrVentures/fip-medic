



We need to change the freqency search terms:
1 capsule every 6 hours -> X every 6 hours

==================== 10_tylenol_gels ====================
Rash was observed as a Indication of 10_tylenol_gels but not as a SideEffect (it was a SideEffect) but should come up for both

==================== 11_backaid_max ====================
"2 caplet" or "Two (2) caplets" --> "2 caplets"

Create a new term "pain" using "muscular pain" and use it if we find pain but dont find a type of pain

"strain" --> "pain"
"alcohol" --> "do not take with alcohol"

==================== 12_aleve_back_and_muscle ====================
Again I see Rash as an indication instead of a side effect

Block out sections based on headers we read such as 
Uses --> Indications
Warnings / SideEffects --> Precautions / SideEffects
Do not use -->

Literally no DoseRoute here so if we cannot determine DoseRoute then look for "tablet" (or capsule, caplet, etc.) and generate "1 tablet" for DR with "as required" for the frequency.


==================== 13_headache_relief ====================
Geltab --> Capsule

==================== 14_migraine_relief ====================
"more than 2 caplets in 24 hours, which is the maximum iu
daily amount" --> 2 per day

Not seeing rash come up in SE

How to convey "Every day"? Let's try "Every morning"
migraine --> headache

==================== 15_aleve_gels ====================
Very tricky Frequency. 
Add terms for every 8 hours which will later become in the morning and at night
if you see "reduces X" then it is an indication

==================== 16_melatonin ====================
gummies --> Capsule
bedtime --> Night
"to help sleep faster and sleep longer" --> "insomnia"

==================== 17_sleep_aid_max ====================
softgels --> Capsule
bedtime --> Night
"sleeplessness" --> "insomnia"
"avoid alcohol" --> "do no take with alcohol"
"1 softgel" --> "1 capsule"

==================== 18_sleep_aid ====================
mini-caplet --> caplet
avoid alcohol --> do not drink alcohol
add a term for drowsiness as a side effect
add a term for pain (general)

==================== 19_advil_pm ====================
Found more indications then desired.
"avoid alcohol" --> "do no take with alcohol"

==================== 1_nyquil ====================
"3 or more alcoholic drinks" --> "do no take with alcohol"
Consult a doctor if vomiting should NOT put vomiting as an expected SE

==================== 20_motrin_pm ====================
remove the comma at the end of the list
I am seeing "take with food if stomach upset", should this be "take with food"?

==================== 21_eye_itch_relief ====================
add term for Indication "itchy eye"
add term for DR "one drop"
"twice daily" --> morning and night

==================== 22_tums_antacid ====================
2-3 tablets --> 2 tablets

==================== 23_cvs_acid_reducer ====================
If we see "tablet" and have no DR then just select "1 tablet"
Again, we see heartburn and dizziness as SE we do NOT want (ask regis on this)

==================== 24_durex_max ====================
"One (1) softgel capsule after breakfast" --> 1 capusle in morning
breakfast --> morning
lunch --> noon
dinner --> night
^ if you see a meal, add "Take with food"
if you see "capsule" and have no DR, just select "1 capsule"
Add indication term for "Bloat"

==================== 25_nasal_allergy_spray ====================
Add term for "Nasal congestion"
Add term for "Runny nose"
Add term for "Sneezing"
Add term for "Itchy nose"
once daily --> "in the morning"
Add DR term for "1 spray" and "2 sprays"
"spray 2 times" --> "2 sprays"

==================== 26_allergy_tablet ====================
--

====================  27_claritin ====================
"daily" -> morning (clear with regis that this shouldnt be in noon, with twice daily being morning and night)


==================== 28_allergy_cetirizine ====================
"once per day" --> morning

==================== 29_benadryl_allergy ====================
"1 to 2 tablets" --> 1 tablet (ask regis)
every 4-6 hours --> every 6 hours (ask regis) pretty sure he said specify the max but say "as needed"
change frequency from "X every 4 hours" to "every 4 hours"
add term "Protect from Light"
store between "20-25C" means "refrigerate"? investigate this...
Why is no alcohol not working here?? (Luke)

==================== 2_ibuprofen ====================
--

==================== 30_nasal_sinus_relief ====================
"sinus congestion" --> "Nasal congestion"
"2 or 3 sprays" --> 2 sprays

==================== 31_claritin_chewables ====================
--

==================== 32_xyzal_allergy ====================
If there is a once daily and another term already (evening / noon), don't add morning (luke)

==================== 33_nasal_saline_mist ====================
This image is bad.


==================== 34_allegra_tablets ====================
"once a day" --> "morning"

==================== 35_cortizone_itch ====================
make a new term that is the rash image but says "itch" (regis)
itchy --> rash
eczema --> rash
"3 to 4 times daily" --> "morning, noon, night"

==================== 36_dimetapp_cold_cough ====================
Didn't list 20 mL even though its in the text
Take smallest mL if multiple (Luke)

==================== 37_mucinex_night ====================
Got some wrong terms here because they are not expected side effects.
Want to contact doctor if they occur. Should we list as SE? (Regis).
If not, this can only really be improved by segmenting the text

==================== 38_benadryl_itch_stop ====================
Not more than bug (luke)

==================== 39_wart_removal ====================
add term "Wart" (indication)

==================== 3_advil ====================
make sure every 4 hours is getting caught (luke)

==================== 40_coricidin_high_blood_pressure ====================
new term "high blood pressure" (indication)


==================== 41_daytime_cold_flu ====================
This photo is missing something reject it from our analsis I messed up

==================== 42_adult_congestion_relief ====================
why no alcohol being captured?? (luke)

==================== 4_ibuprofen ====================
softgel --> capsule

==================== 5_childrens_advil ====================
--

==================== 6_ibuprofen_gels ====================
Reported "crush" when it was actually "do not crush", need an explicit check for this

==================== 7_tylenol ====================
--

==================== 8_acetaminophen ====================
--

==================== 9_tylenol_8hr ====================
--