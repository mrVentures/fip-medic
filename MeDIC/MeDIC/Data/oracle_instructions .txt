Example 10_tylenol_gels extracted terms...
=======>  DoseRoute
2 capsules 
=======>  Frequency
every 4 hours
=======>  Indications
fever, headache 
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
rash


Example 11_backaid_max extracted terms...
=======>  DoseRoute
2 caplets
=======>  Frequency
every 6 hours
=======>  Indications
Muscular pain
=======>  Precautions
Do not take with alcohol or drug
=======>  SideEffects
Rash


Example 12_aleve_back_and_muscle extracted terms...
=======>  DoseRoute
1 tablet
=======>  Frequency
--None--
=======>  Indications
fever, headache, heartburn,  
=======>  Precautions
take with food, 
=======>  SideEffects
rash,


Example 13_headache_relief extracted terms...
=======>  DoseRoute
2 geltabs
=======>  Frequency
--None--
=======>  Indications
headache
=======>  Precautions
--None--
=======>  SideEffects
insomnia


Example 14_migraine_relief extracted terms...
=======>  DoseRoute
2 caplets 
=======>  Frequency
In the Morning
=======>  Indications
coughing, fever, headache, heartburn, nausea, rash, vomiting, 
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
rash, insomnia


Example 15_aleve_gels extracted terms...
=======>  DoseRoute
1 capsule
=======>  Frequency
every 8 hours
=======>  Indications
headache, fever
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
rash


Example 16_melatonin extracted terms...
=======>  DoseRoute
2 Capsules
=======>  Frequency
Night
=======>  Indications
insomnia
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 17_sleep_aid_max extracted terms...
=======>  DoseRoute
1 capsule
=======>  Frequency
night, 
=======>  Indications
insomnia, 
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 18_sleep_aid extracted terms...
=======>  DoseRoute
2 caplets, 
=======>  Frequency
night, 
=======>  Indications
insomnia, 
=======>  Precautions
do not drive, keep out of reach of children, no alcohol
=======>  SideEffects
drowsiness


Example 19_advil_pm extracted terms...
=======>  DoseRoute
2 caplets, 
=======>  Frequency
night, 
=======>  Indications
insomnia
=======>  Precautions
keep out of reach of children, avoid alcohol
=======>  SideEffects
--None--


Example 1_nyquil extracted terms...
=======>  DoseRoute
30 ml, 
=======>  Frequency
night, 
=======>  Indications
fever, headache
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
rash


Example 20_motrin_pm extracted terms...
=======>  DoseRoute
2 caplets, 
=======>  Frequency
night, 
=======>  Indications
pain, insomnia
=======>  Precautions
do not drive, keep out of reach of children, take with food, no alcohol
=======>  SideEffects
--None--


Example 21_eye_itch_relief extracted terms...
=======>  DoseRoute
1 drop
=======>  Frequency
morning, night
=======>  Indications
itchy eye
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 22_tums_antacid extracted terms...
=======>  DoseRoute
2 tablets
=======>  Frequency
--None--
=======>  Indications
heartburn
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 23_cvs_acid_reducer extracted terms...
=======>  DoseRoute
1 tablet
=======>  Frequency
--None--
=======>  Indications
heartburn
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 24_durex_max extracted terms...
=======>  DoseRoute
1 capsule
=======>  Frequency
morning
=======>  Indications
bloat
=======>  Precautions
keep out of reach of children, take with food
=======>  SideEffects
--None--


Example 25_nasal_allergy_spray extracted terms...
=======>  DoseRoute
2 sprays
=======>  Frequency
morning
=======>  Indications
runny nose, nasal congestion, sneezing, Itchy nose
=======>  Precautions
keep out of reach of children, shake, 
=======>  SideEffects
--None--


Example 26_allergy_tablet extracted terms...
=======>  DoseRoute
1 tablet
=======>  Frequency
--None--
=======>  Indications
fever, sneezing, runny nose, itchy eye, itchy nose
=======>  Precautions
--None--
=======>  SideEffects
--None--


Example 27_claritin extracted terms...
=======>  DoseRoute
1 tablet
=======>  Frequency
morning
=======>  Indications
fever, sneezing, runny nose, itchy eye, itchy nose
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 28_allergy_cetirizine extracted terms...
=======>  DoseRoute
1 tablet
=======>  Frequency
morning
=======>  Indications
fever, sneezing, runny nose, itchy eye, itchy nose
=======>  Precautions
keep out of reach of children, do not take with alcohol or drug
=======>  SideEffects
--None--


Example 29_benadryl_allergy extracted terms...
=======>  DoseRoute
1 tablet
=======>  Frequency
every 4 hours
=======>  Indications
fever, sneezing, runny nose, itchy eye, itchy nose
=======>  Precautions
do not take with alcohol or drug
=======>  SideEffects
--None--


Example 2_ibuprofen extracted terms...
=======>  DoseRoute
1 caplet
=======>  Frequency
every 4 hours
=======>  Indications
fever, headache, 
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 30_nasal_sinus_relief extracted terms...
=======>  DoseRoute
2 sprays
=======>  Frequency
every 4 hours
=======>  Indications
fever, nasal congestion
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 31_claritin_chewables extracted terms...
=======>  DoseRoute
2 tablets 
=======>  Frequency
--None--
=======>  Indications
fever, sneezing, runny nose, itchy eye, itchy nose
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 32_xyzal_allergy extracted terms...
=======>  DoseRoute
5 mL
=======>  Frequency
evening, 
=======>  Indications
fever, sneezing, runny nose, itchy eye, itchy nose
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 33_nasal_saline_mist extracted terms...
=======>  DoseRoute
--None--
=======>  Frequency
--None--
=======>  Indications
nasal congestion
=======>  Precautions
--None--
=======>  SideEffects
--None--


Example 34_allegra_tablets extracted terms...
=======>  DoseRoute
1 tablet 
=======>  Frequency
morning
=======>  Indications
fever, sneezing, runny nose, itchy eye, itchy nose
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 35_cortizone_itch extracted terms...
=======>  DoseRoute
apply to affected area, 
=======>  Frequency
morning, noon, night
=======>  Indications
rash, 
=======>  Precautions
--None--
=======>  SideEffects
--None--


Example 36_dimetapp_cold_cough extracted terms...
=======>  DoseRoute
20 mL
=======>  Frequency
every 4 hours
=======>  Indications
fever
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 37_mucinex_night extracted terms...
=======>  DoseRoute
10 ml, 
=======>  Frequency
every 4 hours
=======>  Indications
fever, headache, nasal congestion
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
rash, 


Example 38_benadryl_itch_stop extracted terms...
=======>  DoseRoute
--None--
=======>  Frequency
--None--
=======>  Indications
rash, 
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 39_wart_removal extracted terms...
=======>  DoseRoute
--None--
=======>  Frequency
--None--
=======>  Indications
wart
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
--None--


Example 3_advil extracted terms...
=======>  DoseRoute
1 tablet
=======>  Frequency
every 4 hours
=======>  Indications
headache
=======>  Precautions
keep out of reach of children, take with food, 
=======>  SideEffects
rash


Example 40_coricidin_high_blood_pressure extracted terms...
=======>  DoseRoute
15 ml 
=======>  Frequency
--None--
=======>  Indications
fever, headache,  
=======>  Precautions
--None--
=======>  SideEffects
rash


Example 41_daytime_cold_flu extracted terms...
=======>  DoseRoute
15 ml, 
=======>  Frequency
--None--
=======>  Indications
fever, headache, rash, 
=======>  Precautions
--None--
=======>  SideEffects
--None--


Example 42_adult_congestion_relief extracted terms...
=======>  DoseRoute
20 ml, 
=======>  Frequency
--None--
=======>  Indications
headache, nasal congestion
=======>  Precautions
keep out of reach of children, do not take with alcohol or drug
=======>  SideEffects
rash


Example 43_tylenol_severe extracted terms...
=======>  DoseRoute
2 caplets, 
=======>  Frequency
--None--
=======>  Indications
dizziness, fever, headache, rash, 
=======>  Precautions
crush, do not crush, keep out of reach of children, 
=======>  SideEffects
--None--


Example 4_ibuprofen extracted terms...
=======>  DoseRoute
1 tablet 
=======>  Frequency
every 4 hours
=======>  Indications
heartburn
=======>  Precautions
keep out of reach of children, take with food, 
=======>  SideEffects
rash


Example 5_childrens_advil extracted terms...
=======>  DoseRoute
10 mL
=======>  Frequency
every 6 hours
=======>  Indications
fever, headache
=======>  Precautions
keep out of reach of children, shake, take with food, 
=======>  SideEffects
rash


Example 6_ibuprofen_gels extracted terms...
=======>  DoseRoute
1 capsule 
=======>  Frequency
every 4 hours 
=======>  Indications
fever, headache
=======>  Precautions
do not crush, keep out of reach of children, take with food, 
=======>  SideEffects
--None--


Example 7_tylenol extracted terms...
=======>  DoseRoute
2 caplets
=======>  Frequency
every 6 hours
=======>  Indications
headache
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
rash


Example 8_acetaminophen extracted terms...
=======>  DoseRoute
2 caplets 
=======>  Frequency
every 6 hours
=======>  Indications
fever, headache, 
=======>  Precautions
keep out of reach of children, 
=======>  SideEffects
rash


Example 9_tylenol_8hr extracted terms...
=======>  DoseRoute
2 caplets
=======>  Frequency
every 8 hours
=======>  Indications
headache 
=======>  Precautions
do not crush, 
=======>  SideEffects
--None--


