BROMPHENIRAMINE MALEATE (Antihistamine)
DEXTROMETHORPHAN HBr (Cough Suppressant)
PHENYLEPHRINE HCI (Nasal Decongestant)

& Stuffy Nose
& Runny Nose
& Sneezing

Plus Other Symptoms

& Itchy, Watery Eyes

For Ages

6 Yrs.
& Over

4FLOZ (118 ml) alcohol-free © grape flavor -,

 

Drug Facts

Active ingredients

(in each 10 ml) Purposes
Brompheniramine maleate, USP 2 mg Antihistamine
Dextromethorphan HBr, USP 10 mg...Cough suppressant
Phenylephrine HCI, USP 5 mg Nasal decongestant

Uses

m temporarily relieves cough due to minor throat and
bronchial irritation occurring with a cold, and nasal
congestion due to the common cold, hay fever or
other upper respiratory allergies

m temporarily relieves these symptoms due to hay fever
(allergic rhinitis):
m@ runny nose
@ Sneezing
m itchy, watery eyes
m itching of the nose or throat

m temporarily restores freer breathing through the nose

Warnings

Do not use

m to sedate a child or to make a child sleepy

m if you are now taking a prescription monoamine
oxidase inhibitor (MAOI) (certain drugs for
depression, psychiatric, or emotional conditions, or —
Parkinson's disease), or for 2 weeks after stopping the }
MAO! drug. If you do not know if your prescription
drug contains an MAOI, ask a doctor or pharmacist
before taking this product.

Ask a doctor before use if you have

@ heart disease

@ high blood pressure

m thyroid disease

m@ diabetes

m@ {rouble urinating due to an enlarged prostate gland

@ glaucoma

@ Cough that occurs with too much phlegm (mucus)

m@ a breathing problem or persistent or chronic cough
that lasts such as occurs with Smoking, asthma,
chronic bronchitis, or emphysema

Ask a doctor or pharmacist before use if you are
& taking any other oral nasal decongestant or stimulant
m@ taking sedatives or tranquilizers

 

 

   

Dimet

Drug Facts (continued

When using this product

m (do not use more than directed

@ May Cause marked drowsiness

@ avoid alcoholic beverages

@ alcohol, sedatives, and tranquili
drowsiness

 

vnildren’s
app

Zérs May increase

@ De careful when driving a motor vehicle or Operating

machinery

@ excitability may occur, especially in children

Stop use and ask a doctor if
@ You get nervous, dizzy, or Sleep

less

@ Symptoms do not get better within 7 days or are

accompanied by fever

@ Cough lasts more than / days, comes back, or is
accompanied by fever, rash, or persistent headache.
These could be Signs of a serious condition.

If pregnant or breast-feeding, ask
before use.

a health professional _

Keep out of reach of children. In case of Overdose, get

medical help or contact a Poison C
Directions

ontrol Center right a\ va\

A a

@ do not take more than 6 doses in any 24-hour period
@ Measure only with dosage cup Provided

@ keep dosage cup with product
& ml = milliliter

adults and children
12 years and over
children 6 to under 12 years
children under 6 years

Other information

20 ml Every 4 hours
10 ml every 4 hours

@ each 10 mi contains: sodium 6 mg

M store at 20-25°¢ ( 68-77°F)

Inactive Ingredients anhydrous citric acid, artificial
flavor, FD&C blue no. 1, FD&C red no, 40, glycerin, propylene
glycol, purified water, sodium benzoate, sodium citrate,

SOrbito| Solution, SuCralose
Questions or comments?
9

Call weekdays from

AM to 5 PM EST at 1-800-762-4675

   

Cold « Cough

For most recent product information, visit
www.dimetapp.com

neers See seseiniohinatian
We pledge to you that Dimetapp® product
contain only high quality ing:
and meet strict standards of quality and safety.
You can trust Dimetapp® products for your fz
ee

Dosage Cup Provided

 

w= EP

Learn about teen medicine abuse

    

www.StopMedicineAbuse.org

Distributed by:
Pfizer, Madison, NJ 07940 USA
© 2014 Pfizer Inc.

Ma

- O0341 2234 132

