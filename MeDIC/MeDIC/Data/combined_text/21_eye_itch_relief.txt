 

   

Purpose
Antihistamine J

)

     
    
 

  

Use

Temporarily relieves itchy eyes due to pollen, ragweed
grass, animal hair and dander.

    
   
   
  

Warnings
For external use only
Do not use

z i solution changes color or becomes cloudy
l@ if you are sensitive to any ingredient in this product
@ io treat contact lens related irritation

 
   
   
       
    

When using this product
@ do not touch tip of container to any surface to avoid
contamination mi remove contact lenses before use
@ wait at least 10 minutes before reinserting contact
lenses after use Mi replace cap after each use

  

 
 
  

     
 

    
 

     
  

 

Stop use and ask a doctor if you
experience any of the following:

mM eye pain m changes in vision mM redness of the eye
@ itching worsens or lasts more than 72 hours

Keep out of reach of children.
lf swallowed, get medical help or contact
a Poison Control Center right away.

 
 

        
     
    

  
 
 
  
 

 
  

Directions
@ Adults and children 3 years of age and older:
Put 1 drop in the affected eye(s) twice daily, every
8 to 12 hours, no more than twice per day.

@ Children under 3 years of age: Consult a doctor.

Questions or comments?
1-800-932-5676
Serious side effects associated with use of this produc
may be reported to this number

 
     
 
  

 
    
 
    

