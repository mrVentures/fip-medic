indoor & Outdoor
Allergies

 

:
aver) .*Whei taken as directed,
See Drug Facts Panel, CHEWABLE TABLETS |
aU. ;

 

 

Drug Facts

Active ingredient (in each tablet) Purpose

Loratadine 5 mg
Uses

Antihistamine

temporarily relieves these symptoms due to

hay fever or other upper respiratory allergies:

= runny nose
= sneezing

Warnings
3 Do not use if you have ever had an allergic reaction to
# this product or any of its ingredients.

Ask a doctor before use if you have liver or kidney
# disease. Your doctor should determine if you need a
different dose.

7 When using this product do not take more than
# directed. Taking more than directed may cause
drowsiness.

| Stop use and ask a doctor if an allergic reaction to
# this product occurs. Seek medical help right away.

| lf pregnant or breast-feeding, ask a health

a professional before use.

Keep out of reach of children. In case of overdose,
get medical help or contact a Poison Control Center
right away.

= itchy, watery eyes
= itching of the nose or throat

Directions
adults and children
6 years and over

children 2 to under
6 years of age

children under
2 years of age

consumers with liver
or kidney disease

Other information

= phenylketonurics: contains phenylalanine 1.4 mg
per tablet

= Safety sealed: do not use if the individual blister unit
imprinted with Children’s Claritin® is open or torn

= store between 20° to 25°C (68° to 77°F)

Inactive ingredients

aspartame, citric acid anhydrous, colloidal silicon dioxide,
D&C red No. 27 aluminum lake, FD&C blue No. 2 aluminum §
lake, flavor, magnesium stearate, mannitol, microcrystalline
cellulose, sodium starch glycolate, stearic acid

Questions or comments?
1-800-CLARITIN (1-800-252-7484) or www.claritin.com

chew 2 tablets daily; not r
than 2 tablets in 24 hours

chew 1 tablet daily; not more
than 1 tablet in 24 hours

ask a doctor

ask a doctor

 

 

